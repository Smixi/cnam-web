import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UploadPageComponent} from 'src/app/upload-page/upload-page.component'
import { ListAnnotedComponent } from './list-annoted/list-annoted.component';
const routes: Routes = [
  {"path": "upload", "component": UploadPageComponent},
  {"path": "explore", "component": ListAnnotedComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
}
