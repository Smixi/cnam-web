import { TestBed } from '@angular/core/testing';

import { AzureserviceService } from './azureservice.service';

describe('AzureserviceService', () => {
  let service: AzureserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AzureserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
