import { Component, OnInit } from '@angular/core';
import { AzureserviceService, ApiAnnotedImg } from '../azureservice.service';
import { ConfigLoaderService } from '../config-loader.service';
import { BlobServiceClient, BlockBlobClient, ContainerClient } from '@azure/storage-blob'

@Component({
  selector: 'app-upload-page',
  templateUrl: './upload-page.component.html',
  styleUrls: ['./upload-page.component.scss']
})
export class UploadPageComponent implements OnInit {
  

  constructor(public config: ConfigLoaderService){
  }

  ngOnInit(): void {}

  upload(event: any){
    this.uploadedState = 'not uploaded';
    const blobServiceClient = BlobServiceClient.fromConnectionString(this.config.connecionString)
    const file = event.target.files[0]
    const filename = file.name
    const containerClient = blobServiceClient.getContainerClient(this.config.containerName)
    const blockBlobClient = containerClient.getBlockBlobClient(filename)
    console.log(file, file.size)
    const responseUpload = blockBlobClient.upload(file, file.size).then(()=>{
      this.uploadedState = 'uploaded';
    }).catch(()=>{
      this.uploadedState = 'failed'
    })
  }

  uploadedState: 'not uploaded' | 'uploaded' | 'failed' = 'not uploaded'

}
