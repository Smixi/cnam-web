import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigLoaderService } from './config-loader.service';

export interface ApiAnnotedImg {
  filename: string;
  annotations: string;
}

@Injectable({
  providedIn: 'root'
})
export class AzureserviceService {

  constructor(private httpClient: HttpClient,
              private configLoader: ConfigLoaderService) {

  }

  getListOfImages(){
    return this.httpClient.get<ApiAnnotedImg[]>(this.configLoader.annotedEndpoint)
  }

}
