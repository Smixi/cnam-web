import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { AppConfig } from './config.model';

@Injectable()
export class ConfigLoaderService {

public annotedEndpoint = '';
public storageEndpoint = '';
public connecionString = '';
public containerName = ''

  constructor(private httpClient: HttpClient) { }

  initialize() {
    return this.httpClient.get<AppConfig>('./assets/config.json')
    .pipe(tap((response: AppConfig) => {
      this.annotedEndpoint = response.imageListEndpoint;
      this.storageEndpoint = response.storageRoot;
      this.connecionString = response.connectionString;
      this.containerName = response.containerName;
    })).toPromise<AppConfig>();
  }

}