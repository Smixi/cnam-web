import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAnnotedComponent } from './list-annoted.component';

describe('ListAnnotedComponent', () => {
  let component: ListAnnotedComponent;
  let fixture: ComponentFixture<ListAnnotedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListAnnotedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAnnotedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
