import { Component, OnInit } from '@angular/core';
import { ApiAnnotedImg, AzureserviceService } from '../azureservice.service';
import { ConfigLoaderService } from '../config-loader.service';

@Component({
  selector: 'app-list-annoted',
  templateUrl: './list-annoted.component.html',
  styleUrls: ['./list-annoted.component.scss']
})
export class ListAnnotedComponent implements OnInit {

  imgUrl = '';
  annoted: ApiAnnotedImg[] = [];

  constructor(public config: ConfigLoaderService,
    private azureService: AzureserviceService) {
      this.imgUrl = config.storageEndpoint
  }

  ngOnInit(): void {
    this.azureService.getListOfImages().subscribe(annotedList => {
      this.annoted = annotedList
    })
  }

}
