export interface AppConfig {
  imageListEndpoint: string;
  storageRoot: string;
  connectionString: string,
  containerName: string,
  }
  