
# Stage 1
FROM node:16-alpine as build-step
RUN mkdir -p /app
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
RUN npm run build --prod
# Stage 2
EXPOSE 80
ENV STORAGEROOT
ENV IMAGELISTFUNCTIONENDPOINT
ENV CONTAINERNAME
ENV CONNECTIONSTRING
FROM nginx:1.19-alpine
COPY --from=build-step /app/dist/cnamaidemo /usr/share/nginx/html
COPY generateConfig.sh /docker-entrypoint.d/generateConfig.sh